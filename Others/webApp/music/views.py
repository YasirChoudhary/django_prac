from django.http import HttpResponse, Http404
from .models import Album,Song
from django.shortcuts import render, get_object_or_404

def index(request):
    album_list = Album.objects.all()
    return render(request, 'music/index.html', {'album_list': album_list})


def detail(request, album_id):
    album = get_object_or_404(Album, pk=album_id)
    return render(request, 'music/detail.html', { 'album':album})

def favourite(request, album_id):
    album = get_object_or_404(Album, pk=album_id)
    try:
        selected_song = album.song_set.get(pk=request.POST['song'])
    except (KeyError,Song.DoesNotExist):
        return render(request, 'music/detail.html', {
            'album':album,
            'error_message': "You didn't select a valid song",
        })
    else:
        selected_song.is_is_favourite = True
        selected_song.save()
        return render(request, 'music/detail.html', {'album':album} )
